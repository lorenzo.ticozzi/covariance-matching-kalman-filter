% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
function [z_hat,P_opt,P_hat,CovEst] = AKF(F,H,z,y,m,Q,R,P0,var)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %

p = size(y,1);    % size of the measurements vector
l = size(z,1);    % size of the observable subspace of the state

% - Initialize quantity for the equation Y(k) = Mo*z(k) + Mw*W(k) + V(k) 
Mo = zeros(p*m,l);                    % ok
Mw = zeros(p*m,l*(m-1));              % ok
Yk = zeros(p*m,size(y,2)-m+1);        % ok
Zk = zeros(l,size(Yk,2)-1);           % ok
Lambdak = zeros(l,l,size(Zk,2));      % ok

% - Fill the previous matrices

% Mo
row = 0;
for mm = m-1 : -1 : 0                  
    Mo(row+1:row+p,:) = H*(F^mm);
    row = row+p;
end                                   % ok

Mo_pinv = pinv(Mo);  % Moore-Penrose pseudoinverse of Mo

% Mw
mm = 0;

for row = 0 : p : p*(m-2)              
    for col = row/p*l : l : l*(m-2)  
        Mw(row+1:row+p,col+1:col+l) = H*F^mm;
        mm = mm+1;
    end
    mm = 0;
end                                   % ok

% Yk
mm = m;
for row = 0 : p : p*(m-1)
    Yk(row+1:row+p,:) = y(:,mm:mm+size(Yk,2)-1);
    mm = mm-1;
end                                   % ok 

% Zk
for k = 1 : size(Yk,2)-1
    Zk(:,k) = Mo_pinv*Yk(:,k+1) - F*Mo_pinv*Yk(:,k);
end

% Lambdak
% for k = 1 : size(Zk,2)
%     if k == 1
%         Lambdak(:,:,k) = Zk(:,k)*Zk(:,k)'
%     else
%         Lambdak(:,:,k) = (k-1)/k*Lambdak(:,:,k-1) + 1/k*Zk(:,k)*Zk(:,k)'
%     end
% end

% - Estimate Cov(Zk)
LambdaPre = 0;
for k = 1 : size(Zk,2)
    Lambdak(:,:,k)= (k-1)/k*LambdaPre + 1/k*Zk(:,k)*Zk(:,k)';
    LambdaPre = Lambdak(:,:,k);
end

% ZkOld = zeros(size(Zk,1),size(Zk,1));
% for k = 1 : size(Zk,2)
%     ZkNew = ZkOld + Zk(:,k)*Zk(:,k)';
%     LambdakTry(:,:,k) = 1/k * ZkNew;
%     ZkOld = ZkNew;
% end



% Build matrices A,B
A = [Mo_pinv*Mw eye(l)] - [zeros(l,l) F*Mo_pinv*Mw];
B = [Mo_pinv zeros(l,p)] - [zeros(l,p) F*Mo_pinv];


if isequal(var,Q)
    
    %%%%%%%%%%%% - Find Rk given full knowledge of Q matrix - %%%%%%%%%%%
    % Build matrix Ck and vectorize it
    
    WCovAll = zeros(l,l,m-1);
    
    k = 1;
    for col = l*(m-1) : -l : 0
        WCovAll(:,:,k) = A(:,col+1:col+l)*Q*A(:,col+1:col+l)';
        k = k+1;
    end
    WCov = sum(WCovAll,3);
    
    Ck = zeros(size(Lambdak));
    CkVec = zeros(size(Ck,1)*size(Ck,2),size(Ck,3));
    
    for k = 1 : size(Lambdak,3)
        Ck(:,:,k) = Lambdak(:,:,k) - WCov;
    end
    
    
    for k = 1 : size(Ck,3)          % vectorize matrices Ck
        for i = 1 : size(Ck,1)
            for j = 1 : size(Ck,2)
                ll = size(Ck,1)*(i-1)+j;
                CkVec(ll,k) = Ck(i,j,k);
            end
        end
    end
    
    % Assemble S matrix
    S_all = zeros(l*l,p*p,m+1);
    col = size(B,2)-p;
    
    for k = 1 : size(S_all,3)
        S_all(:,:,k) = kron(B(:,col+1:col+p),B(:,col+1:col+p));
        col = col-p;
    end
    
    % Compute the estimate of the R matrix
    S_opt = sum(S_all,3);
    S_pinv = pinv(S_opt);
    
    Rk = zeros(p,p,size(CkVec,2));   % Initialize the noise covariance matrix
    
    for k = 1 : size(CkVec,2)
        RkVec = S_pinv*CkVec(:,k);
        RkMat = [RkVec(1) RkVec(2) ;
                 RkVec(3) RkVec(4)];
        [~,p] = chol(RkMat);
        
        if p ~= 0 && k > 1                  % Check that Rk is positive definite
            Rk(:,:,k) = Rk(:,:,k-1);
        else
            Rk(:,:,k) = RkMat;
        end
        
    end
    
    CovEst = Rk;
    
    %%%%%%%%%%% - Run the K�lman filter - %%%%%%%%%%%%%
    % - Initialize variables
    z_hat = zeros(l,size(Rk,3));
    z_hat(:,1) = z(:,1);
    
    P_opt = zeros(size(z,1),size(z,1),size(Rk,3));
    P_hat = zeros(size(z,1),size(z,1),size(Rk,3));
    
    P_opt(:,:,1) = P0;
    P_hat(:,:,1) = P0;
    
    % - Initialize the Kalmàn gain
    K = zeros(size(z,1),size(y,1),size(z,2));
    K_hat = zeros(size(K));
    
    for k = 2 : size(Rk,3)
        % - Prediction step
        z_hat(:,k) = F * z_hat(:,k-1);                    % x[k|k-1] = F * x[k-1|k-1]
        P_opt(:,:,k) = F * P_opt(:,:,k-1) * F' + Q;       % P[k|k-1] = F*P[k-1|k-1]*F' + Q
        P_hat(:,:,k) = F * P_hat(:,:,k-1) * F' + Q;
        
        % - Update step
        S = H*P_opt(:,:,k)*H' + R;    % Use known meas. noise covariance matrix
        S_hat = H*P_hat(:,:,k)*H' + Rk(:,:,k);
        S_inv = S \ eye(size(S,1));
        S_hat_inv = S_hat \ eye(size(S_hat,1));
        
        K(:,:,k) = P_opt(:,:,k) * H' * S_inv;                      % K[k] = P[k|k-1]*H' * (H*P[k|k-1]*H' + R)^(-1)
        K_hat(:,:,k) = P_hat(:,:,k) * H' * S_hat_inv;
        
        z_hat(:,k) = z_hat(:,k) + K(:,:,k)*(y(:,k)-H*z_hat(:,k));  % x[k|k] = x[k|k-1] + K[k]*(y[k]-H*x[k|k-1])
        P_opt(:,:,k) = P_opt(:,:,k) - K(:,:,k)*H*P_opt(:,:,k);     % P[k|k] = P[k|k-1] - K[k]*H*P[k|k-1]
        P_hat(:,:,k) = P_hat(:,:,k) - K_hat(:,:,k)*H*P_hat(:,:,k);
        
    end
    
elseif isequal(var,R)
    %%%%%%%%%%%% - Find Qk given full knowledge of R matrix - %%%%%%%%%%%
    VCovAll = zeros(l,l,m+1);
    
    k = 1;
    for col = m*p : -p : 0
        VCovAll(:,:,k) = B(:,col+1:col+p)*R*B(:,col+1:col+p)';
        k = k+1;
    end
    
    VCov = sum(VCovAll,3);
    
    Dk = zeros(size(Lambdak));
    DkVec = zeros(size(Lambdak,1)*size(Lambdak,2),size(Lambdak,3));
    T_all = zeros(l*l,l*l,m);
    Qk = zeros(l,l,size(DkVec,2));
    
    for k = 1 : size(Dk,3)
        Dk(:,:,k) = Lambdak(:,:,k) - VCov;
    end
    
    for k = 1 : size(Dk,3)          % vectorize matrices Ck
        for i = 1 : size(Dk,1)
            for j = 1 : size(Dk,2)
                ll = size(Dk,1)*(i-1)+j;
                DkVec(ll,k) = Dk(i,j,k);
            end
        end
    end
    
    col = size(A,2)-l;
    for k = 1 : size(T_all,3)
        T_all(:,:,k) = kron(A(:,col+1:col+l),A(:,col+1:col+l));
        col = col-l;
    end
    
    T = sum(T_all,3);
    T_pinv = pinv(T);
    
    for k = 1 : size(DkVec,2)
        QkVec = T_pinv*DkVec(:,k);
        QkMat = [QkVec(1) QkVec(2) QkVec(3) ;
            QkVec(4) QkVec(5) QkVec(6) ;
            QkVec(7) QkVec(8) QkVec(9)];
        [~,p] = chol(QkMat);
        
        if p ~= 0 && k > 1           % Check that Qk is positive definite
            Qk(:,:,k) = Qk(:,:,k-1);
        else
            Qk(:,:,k) = QkMat;
        end
        
    end
    
    CovEst = Qk;
    
    %%%%%%%%%%% - Run the Kàlman filter - %%%%%%%%%%%%%
    % - Initialize variables
    z_hat = zeros(l,size(Qk,3));
    z_hat(:,1) = z(:,1);
    
    P_opt = zeros(size(z,1),size(z,1),size(Qk,3));
    P_hat = zeros(size(z,1),size(z,1),size(Qk,3));
    
    P_opt(:,:,1) = P0;
    P_hat(:,:,1) = P0;
    
    % - Initialize the Kalmàn gain
    K = zeros(size(z,1),size(y,1),size(z,2));
    K_hat = zeros(size(K));
    
    for k = 2 : size(Qk,3)
        % - Prediction step
        z_hat(:,k) = F * z_hat(:,k-1);                    % x[k|k-1] = F * x[k-1|k-1]
        P_opt(:,:,k) = F * P_opt(:,:,k-1) * F' + Q;       % P[k|k-1] = F*P[k-1|k-1]*F' + Q
        P_hat(:,:,k) = F * P_hat(:,:,k-1) * F' + Qk(:,:,k);
        
        % - Update step
        S = H*P_opt(:,:,k)*H' + R;    % Use known meas. noise covariance matrix
        S_hat = H*P_hat(:,:,k)*H' + R;
        S_inv = S \ eye(size(S,1));
        S_hat_inv = S_hat \ eye(size(S_hat,1));
        
        K(:,:,k) = P_opt(:,:,k) * H' * S_inv;                      % K[k] = P[k|k-1]*H' * (H*P[k|k-1]*H' + R)^(-1)
        K_hat(:,:,k) = P_hat(:,:,k) * H' * S_hat_inv;
        
        z_hat(:,k) = z_hat(:,k) + K(:,:,k)*(y(:,k)-H*z_hat(:,k));  % x[k|k] = x[k|k-1] + K[k]*(y[k]-H*x[k|k-1])
        P_opt(:,:,k) = P_opt(:,:,k) - K(:,:,k)*H*P_opt(:,:,k);     % P[k|k] = P[k|k-1] - K[k]*H*P[k|k-1]
        P_hat(:,:,k) = P_hat(:,:,k) - K_hat(:,:,k)*H*P_hat(:,:,k);
        
    end
    
end



% % - Create Wk,Vk as symbolic variables
% syms Q_11 Q_22
% 
% Q0 = [Q_11 0.04 0.04 ;
%       0.04 Q_22 0.04 ;
%       0.04 0.04 0.25];  % Covariance matrix of the process noise
%  
% R0 = [R_11 0    ;
%       0    0.25];
% 
% WkCov = zeros(l,l);
% VkCov = zeros(l,l);
% 
% for col = l*(m-1) : -l : 0
%     WkCovNew = A(:,col+1:col+l)*Q0*A(:,col+1:col+l)';
%     WkCov = WkCov + WkCovNew;
% end
% 
% for col = m*p : -p : 0
%     VkCovNew = B(:,col+1:col+p)*R0*B(:,col+1:col+p)';
%     VkCov = VkCov + VkCovNew;
% end
% 
% % Solve the matrix system
% for k = 1 : size(Lambdak,3)
% %     eqn = Lambdak(:,:,k) - WkCov - VkCov == zeros(size(WkCov));
%     eqns = [Lambdak(1,1,k)-WkCov(1,1)-VkCov(1,1)==0
%             Lambdak(1,2,k)-WkCov(1,2)-VkCov(1,2)==0
%             Lambdak(1,3,k)-WkCov(1,3)-VkCov(1,3)==0];
%     vars = [Q_11 Q_22 R_11];
%     [SS,b] = equationsToMatrix(eqns,vars);
%     SSnum = double(SS);   bnum = double(b);
%     sol = SSnum\bnum
% end


% - Vectorize the covariance equation
% LambdakVec = zeros(size(Lambdak,1)*size(Lambdak,2),1);
% WkCovVec = zeros(size(WkCov,1)*size(WkCov,2),1);
% VkCovVec = zeros(size(VkCov,1)*size(VkCov,2),1);
% 
% for i = 1 : size(Lambdak,1)
%     for j = 1 : size(Lambdak,2)
%         k = size(Lambdak,1)*(i-1)+j;
%         LambdakVec(k) = Lambdak(i,j);
%         WkCovVec(k) = WkCov(i,j);
%         VkCovVec(k) = VkCov(i,j);
%     end
% end
% 
% eqn = LambdakVec - WkCovVec - VkCovVec == 0;

end







