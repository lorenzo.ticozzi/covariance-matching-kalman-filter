%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Main for the AKF_simulation.slx model - %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
close all
clc


% - Simulate the system behaviour
T = .1;
t = (0 : T : 50)';

% - Initialize quantities
z = zeros(3,length(t));   z(:,1) = [0 0 9.81]'; % Initial value of the state
y = zeros(2,length(t));

% Measure position and velocity
H = [1 0 0 ;
     0 1 0];
 
% % Measure velocity and acceleration
% H = [0 1 0 ;
%      0 0 1];

% - Create the noise matrices
mu_w = [0 0 0]';
mu_v = [0 0]';

% % Enable this data for sensitivity analysis
% R_mag = 1e-2;   % 1st case (unreliable model)
% Q_mag = 1e+2;

% R_mag = 1e+2;   % 2nd case (unreliable measurements)
% Q_mag = 1e-2;

% % Enable these data for standard case
Q_mag = 1e+1;
R_mag = 1e+1;

% Process covariance matrix
Q = Q_mag*[0.25 0     0    ;
           0    0.25  0    ;
           0    0     0.25];

% Error covariance matrix
R = R_mag*[0.25 0    ;
          0    0.25];

P0 = 5*eye(size(z,1));


% Generation of process noise                      %
% var_w = 0.25;                                    % Variance 
% mu_w = 0;                                        % Expected value
% sigma_w = sqrt(var_w);                           % Standard deviation
% w = (mu_w*ones(size(z,1),length(t))+...          % Noise vector for each instant of time 
%     sigma_w*randn(size(z,1),length(t)))';        %
% Q = eye(size(z,1))*var_w;                        % Covariance matrix
%                                                  %
% % Generation of measurement noise                %
% var_v = 0.25;                                    % Variance
% mu_v = 0;                                        % Expected value
% sigma_v = sqrt(var_v);                           % Standard deviation 
% v = (mu_v*ones(size(y,1),length(t))+...          % Noise vector for each instant of time 
%     sigma_v*randn(size(y,1),length(t)))';        % 
% R = eye(size(y,1))*var_v;

rng('shuffle')
% s = rng;
% rng('shuffle')
w = mvnrnd(mu_w,Q,length(t));  % Process noise
v = mvnrnd(mu_v,R,length(t));  % Measurement noise

% - Plot of the process and measurements noise evolution
figure(1)
subplot(211)
plot(t,w(:,1),t,w(:,2),t,w(:,3))
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Process noise','Interpreter','latex','FontSize',20)
legend('$w_{1}, [m]$','$w_{2}, [m/s]$','$w_{3}, [m/s^2]$','Interpreter','latex','FontSize',16)
sgtitle('Process and measurement noise','Interpreter','latex','FontSize',20)
grid minor
subplot(212)
plot(t,v(:,1),t,v(:,2))
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Measurements noise','Interpreter','latex','FontSize',20)
legend('$v_{1}, [m]$','$v_{2}, [m/s]$','Interpreter','latex','FontSize',16)
grid minor

% - Compute the system evolution

F = [1  T  0.5*T^2  ;   % State transition matrix
    0  1     T     ;
    0  0     1    ];

for k = 1 : length(t)
    
    y(:,k) = H*z(:,k) + v(k,:)';  % Measure of the position and veocity
    
    if k < length(t)
        z(:,k+1) = F*z(:,k) + w(k,:)';
    end
    
end

% - Plot the process evolution
figure(2)
plot(t,z(1,:),t,z(2,:),t,z(3,:),'LineWidth',1.5)
lgd = legend({'Position','Velocity','Acceleration'},'Interpreter','latex',...
    'FontSize',16);
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
title('Process evolution','Interpreter','latex','FontSize',20)
grid minor

% Plot the measurements evolution
figure(3)
plot(t,y(1,:),t,y(2,:),'LineWidth',1.5)
legend('Measured position','Measured Velocity','Interpreter','latex',...
    'FontSize',16)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
title('Measurements evolution','Interpreter','latex','FontSize',20)
grid minor



%% - Run the baseline Kalman filter

% - Run the DT Kalm�n�n filter
[z_hat,P_est] = DTKalman(z,y,F,H,Q,R,P0);

P_est11 = squeeze(P_est(1,1,:));
P_est22 = squeeze(P_est(2,2,:));
P_est33 = squeeze(P_est(3,3,:));

% - Plot the reconstructed state vs the measurements output
figure(4)
subplot(311)
plot(t,z(1,:),t,y(1,:),'--',t,z_hat(1,:),'-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('Actual position','Measured position','Estimated position',...
    'Interpreter','latex','FontSize',16,'Location','best')
grid minor
sgtitle('Baseline Kalman filter results','Interpreter','latex','FontSize',20)
subplot(312)
plot(t,z(2,:),t,y(2,:),'--',t,z_hat(2,:),'-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('Actual velocity','Measured velocity','Estimated velocity',...
    'Interpreter','latex','FontSize',16,'Location','best')
grid minor
subplot(313)
plot(t,z(3,:),t,z_hat(3,:),'-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('Actual acceleration','Estimated acceleration',...
    'Interpreter','latex','FontSize',16,'Location','best')
grid minor

% - Plot the state error covariance matrix estimated by the DT Kàlman filter
figure(5)
plot(t,P_est11,t,P_est22,t,P_est33,'LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('$P_{est}$','Interpreter','latex','FontSize',20)
legend('$P_{11}$','$P_{22}$','$P_{33}$','Interpreter','latex','FontSize',16)
title('$P_{11},P_{22},P_{33}$','FontSize',16,'Interpreter','latex','FontSize',20)
grid minor



%% - Run the AKF
% Given Q the filter estimates R, and also the converse is valid. To
% estimate one or the other change the value of the known input in the call
% of the function (last input variable).

m = 5;  % Number of measurements taken into account at every time step

[z_hat,P_opt,P_hat,Rk] = AKF(F,H,z,y,m,Q,R,P0,Q);     % Run this line if R is the unknown covariance matrix
% [z_hat,P_opt,P_hat,Qk] = AKF(F,H,z,y,m,Q,R,P0,R);   % Run this line if Q is the unknown covariance matrix

% Rename state error covariance components
P11_hat = squeeze(P_hat(1,1,:));   P11_opt = squeeze(P_opt(1,1,:));
P22_hat = squeeze(P_hat(2,2,:));   P22_opt = squeeze(P_opt(2,2,:));
P33_hat = squeeze(P_hat(3,3,:));   P33_opt = squeeze(P_opt(3,3,:));

% Get the estimation error
EstErr = zeros(size(z));
for k = 1 : size(z_hat,2)
    EstErr(:,k) = z(:,k)-z_hat(:,k);
end


%% - Rk case
% Run this section if the AKF was used to find R

RkError = zeros(size(Rk,3),1);
RkErrorNorm = zeros(size(Rk,3),1);

for k = 1 : size(Rk,3)
    RkError(k) = norm(R-Rk(:,:,k));
    RkErrorNorm(k) = norm(R-Rk(:,:,k))/norm(R);
end

% Plot the estimation error (Rk case)
figure(6)
subplot(311)
plot(t(1:size(Rk,3)),EstErr(1,1:size(Rk,3))',t(1:size(Rk,3)),3*sqrt(abs(P11_hat)),...
    'k-.',t(1:size(Rk,3)),-3*sqrt(abs(P11_hat)),'k-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('$e_{1}$','Interpreter','latex','FontSize',20)
grid minor
sgtitle('Estimation errors with $3\sigma$ values obtained from $\hat{P}_{k}$',...
    'Interpreter','latex','FontSize',20)

subplot(312)
plot(t(1:size(Rk,3)),EstErr(2,1:size(Rk,3))',t(1:size(Rk,3)),3*sqrt(abs(P22_hat)),...
    'k-.',t(1:size(Rk,3)),-3*sqrt(abs(P22_hat)),'k-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('$e_{2}$','Interpreter','latex','FontSize',20)
grid minor
subplot(313)
plot(t(1:size(Rk,3)),EstErr(3,1:size(Rk,3))',t(1:size(Rk,3)),3*sqrt(abs(P33_hat)),...
    'k-.',t(1:size(Rk,3)),-3*sqrt(abs(P33_hat)),'k-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('$e_{3}$','Interpreter','latex','FontSize',20)
grid minor

figure(7)
subplot(211)
semilogy(t(1:length(RkError)),RkError,'LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('$\mid \mid R-\hat{R}_{k} \mid \mid_{2} $','Interpreter','latex',...
    'FontSize',20)
title(['$\hat{R}_{k}$ estimation error norm - $R_{ij}\sim$',num2str(0.1*R_mag),...
    ', $Q_{ij}\sim$',num2str(0.1*Q_mag), ', m = ',num2str(m)],...
    'Interpreter','latex','FontSize',20)
set(gca,'FontSize',20)

grid on
subplot(212)
semilogy(t(1:length(RkErrorNorm)),RkErrorNorm,'LineWidth',1.5)
xlabel('Time [s]','Interpreter','latex','FontSize',20)
ylabel('$\frac{\mid \mid R-\hat{R}_{k} \mid \mid_{2}}{\mid\mid R \mid\mid_{2}} $',...
    'Interpreter','latex','FontSize',26)
grid on
set(gca,'FontSize',20)

% Plot the components of the state error covariance matrices obtained with 
% different methods
figure(8)
subplot(311)
plot(t(1:size(Rk,3)),P11_opt,'k-',t(1:size(Rk,3)),P11_hat,'r-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('$P_{opt,11}$','$\hat{P}_{11}$','Interpreter','latex',...
    'FontSize',16)
grid minor
sgtitle('Error covariances obtained using $R$ and $\hat{R}_{k}$',...
    'Interpreter','latex','FontSize',20)

subplot(312)
plot(t(1:size(Rk,3)),P22_opt,'k-',t(1:size(Rk,3)),P22_hat,'r-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('$P_{opt,22}$','$\hat{P}_{22}$','Interpreter','latex',...
    'FontSize',16)
grid minor
subplot(313)
plot(t(1:size(Rk,3)),P33_opt,'k-',t(1:size(Rk,3)),P33_hat,'r-.','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
legend('$P_{opt,33}$','$\hat{P}_{33}$','Interpreter','latex',...
    'FontSize',16)
grid minor

%% - Qk case
% Run this section if the AKF was used to find Q

% QkError = zeros(size(Qk,3),1);
% QkErrorNorm = zeros(size(Qk,3),1);
% 
% for k = 1 : size(Qk,3)
%     QkError(k) = norm(Q-Qk(:,:,k));
%     QkErrorNorm(k) = QkError(k)/norm(Q);
% end
% 
% % Plot of the estimation error (Qk error)
% figure(9)
% subplot(311)
% plot(t(1:size(Qk,3)),EstErr(1,1:size(Qk,3))',t(1:size(Qk,3)),3*sqrt(abs(P11_hat)),...
%     'k-.',t(1:size(Qk,3)),-3*sqrt(abs(P11_hat)),'k-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% ylabel('$e_{1}$','Interpreter','latex','FontSize',20)
% % txt = '\leftarrow x = 11.5';
% % text(11.6,-1.569,txt,'VerticalAlignment','top')
% grid minor
% sgtitle('Estimation errors with $3\sigma$ values obtained from $\hat{P}_{k}$',...
%     'Interpreter','latex','FontSize',20)
% 
% subplot(312)
% plot(t(1:size(Qk,3)),EstErr(2,1:size(Qk,3))',t(1:size(Qk,3)),3*sqrt(abs(P22_hat)),...
%     'k-.',t(1:size(Qk,3)),-3*sqrt(abs(P22_hat)),'k-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% ylabel('$e_{2}$','Interpreter','latex','FontSize',20)
% % txt = '\leftarrow x = 11.5';
% % text(11.5,-4.22,txt)
% grid minor
% 
% subplot(313)
% plot(t(1:size(Qk,3)),EstErr(3,1:size(Qk,3))',t(1:size(Qk,3)),3*sqrt(abs(P33_hat)),...
%     'k-.',t(1:size(Qk,3)),-3*sqrt(abs(P33_hat)),'k-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% ylabel('$e_{3}$','Interpreter','latex','FontSize',20)
% % txt = '\leftarrow x = 11.5';
% % text(11.7,-89.69,txt)
% grid minor
% 
% figure(10)
% subplot(211)
% semilogy(t(1:length(QkError)),QkError,'LineWidth',1.5)
% xlabel('Time [s]','Interpreter','latex','FontSize',20)
% ylabel('$\mid \mid Q-\hat{Q}_{k} \mid \mid_{2} $','Interpreter','latex',...
%     'FontSize',20)
% sgtitle(['$\hat{Q}_{k}$ estimation error norm - $R_{ij}\sim$',num2str(0.1*R_mag),...
%     ', $Q_{ij}\sim$',num2str(0.1*Q_mag), ', m = ',num2str(m)],...
%     'Interpreter','latex','FontSize',20)
% set(gca,'FontSize',20)
% % txt = '\leftarrow x = 11.5';
% % text(11.5,38.72,txt)
% grid on
% subplot(212)
% semilogy(t(1:length(QkError)),QkErrorNorm,'LineWidth',1.5)
% xlabel('Time [s]','Interpreter','latex','FontSize',20)
% ylabel('$\frac{\mid \mid Q-\hat{Q}_{k} \mid \mid_{2}}{\mid \mid Q \mid \mid_{2}} $',...
%     'Interpreter','latex','FontSize',26)
% grid on
% set(gca,'FontSize',20)
% 
% % Plot of the state error covariance matrices obtained with different
% % methods (Qk case)
% figure(11)
% subplot(311)
% plot(t(1:size(Qk,3)),P11_opt,'k-',t(1:size(Qk,3)),P11_hat,'r-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% legend('$P_{opt,11}$','$\hat{P}_{11}$','Interpreter','latex',...
%     'FontSize',16)
% % txt = '\leftarrow x = 11.5';
% % text(11.3,7.959,txt)
% grid minor
% set(gca,'FontSize',20)
% sgtitle('Error covariances obtained with $Q$ or $\hat{Q}_{k}$',...
%     'Interpreter','latex','FontSize',20)
% 
% subplot(312)
% plot(t(1:size(Qk,3)),P22_opt,'k-',t(1:size(Qk,3)),P22_hat,'r-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% legend('$P_{opt,22}$','$\hat{P}_{22}$','Interpreter','latex',...
%     'FontSize',16)
% set(gca,'FontSize',20)
% % txt = '\leftarrow x = 11.5';
% % text(11.3,1.979,txt)
% grid minor
% 
% subplot(313)
% plot(t(1:size(Qk,3)),P33_opt,'k-',t(1:size(Qk,3)),P33_hat,'r-.','LineWidth',1.5)
% xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
% legend('$P_{opt,33}$','$\hat{P}_{33}$','Interpreter','latex',...
%     'FontSize',16)
% % txt = '\leftarrow x = 11.5';
% % text(11.3,4307,txt)
% set(gca,'FontSize',20)
% grid minor



%% - Results

% - Estimation of the position
pos_est = z_hat(1,1:end-m);          % filtered position
pos = z(1,1:length(pos_est));        % true response of the system
pos_meas = y(1,1:length(pos_est));

% Compare true values, estimated and measured - position
figure(12)
subplot(211)
plot(t(1:length(pos_est)),pos,t(1:length(pos_est)),pos_est,'--','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Position [m]','Interpreter','latex','FontSize',20)
legend('$z_{1}$','$\hat{z}_{1}$','Interpreter','latex','FontSize',16)
title('Position','Interpreter','latex','FontSize',20)
set(gca,'FontSize',20)
grid minor

subplot(212)
title('Measurement vs estimation error','Interpreter','latex','FontSize',20)
plot(t(1:length(pos_est)),pos-pos_meas,t(1:length(pos_est)),...
    pos-pos_est,'--','LineWidth',1.5),
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Position error [m]','Interpreter','latex','FontSize',20)
legend('Measurement error','Estimation error','Interpreter','latex',...
    'FontSize',16)
set(gca,'FontSize',20)
grid minor

% - Compare the covariances of the position error
MeasErrPos = pos-pos_meas;   % measurement error
MeasErrCovPos = sum(MeasErrPos.*MeasErrPos)/length(MeasErrPos)   % covariance of the measurement error
EstErrPos = pos-pos_est;     % estimation error
EstErrCovPos = sum(EstErrPos.*EstErrPos)/length(EstErrPos)  % covariance of the estimation error


% - Estimation of the velocity
vel_est = z_hat(2,1:end-m);    % filtered position
vel = z(2,1:length(vel_est));            % true response of the system
vel_meas = y(2,1:length(vel_est));

% Compare true values, estimated and measured - velocity
figure(13)
subplot(211), plot(t(1:length(pos_est)),vel,t(1:length(pos_est)),...
    vel_est,'--','LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Velocity [m/s]','Interpreter','latex','FontSize',20)
legend('$z_{2}$','$\hat{z}_{2}$','Interpreter','latex','FontSize',16)
title('Velocity','Interpreter','latex','FontSize',20)
set(gca,'FontSize',20)
grid minor
subplot(212), plot(t(1:length(pos_est)),vel-vel_meas,...
    t(1:length(pos_est)),vel-vel_est,'--','LineWidth',1.5),
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Velocity error [m/s]','Interpreter','latex','FontSize',20)
legend('Measurement error','Estimation error','Interpreter','latex',...
    'FontSize',16)
grid minor
set(gca,'FontSize',20)

% - Compare the covariances of the velocity error
MeasErrVel = vel-vel_meas;
MeasErrCovVel = sum(MeasErrVel.*MeasErrVel)/length(MeasErrVel)
EstErrVel = vel-vel_est;
EstErrCovVel = sum(EstErrVel.*EstErrVel)/length(EstErrVel)

% - Estimation of the acceleration
acc_est = z_hat(3,1:end-m);    % filtered position
acc = z(3,1:length(acc_est));  % true response of the system

% Compare true values and estimated - acceleration
figure(14)
subplot(211), plot(t(1:length(acc_est)),acc,t(1:length(acc_est)),...
    acc_est,'LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Acceleration $[m/s^2]$','Interpreter','latex','FontSize',20)
legend('$z_{3}$','$\hat{z}_{3}$','Interpreter','latex','FontSize',16)
title('Acceleration','Interpreter','latex','FontSize',20)
grid minor
subplot(212), plot(t(1:length(acc_est)),acc-acc_est,'LineWidth',1.5)
xlabel('Sample time [s]','Interpreter','latex','FontSize',20)
ylabel('Acceleration error [m/s]','Interpreter','latex','FontSize',20)
legend('Estimation error','Interpreter','latex','FontSize',16)
grid minor









