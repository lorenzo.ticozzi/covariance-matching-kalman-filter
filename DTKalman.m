% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %
function [x_hat,P_hat] = DTKalman(x,y,F,H,Q,R,P0)
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ %

% - Initialize the vector of state estimates
x_hat = zeros(size(x));
x_hat(:,1) = x(:,1);

% - Initialize the state error covariance matrix
P_hat = zeros(size(x,1),size(x,1),size(x,2));
P_hat(:,:,1) = P0;

% - Initialize the Kalm�n gain
K = zeros(size(x,1),size(y,1),size(x,2));

for k = 2 : size(x,2)
    
    % - Prediction step
    x_hat(:,k) = F * x_hat(:,k-1);                    % x[k|k-1] = F * x[k-1|k-1]
    P_hat(:,:,k) = F * P_hat(:,:,k-1) * F' + Q;       % P[k|k-1] = F*P[k-1|k-1]*F' + Q
    
    % - Update step
    G = H*P_hat(:,:,k)*H' + R;
    G_inv = G \ eye(size(G,1));
    K(:,:,k) = P_hat(:,:,k) * H' * G_inv;                      % K[k] = P[k|k-1]*H' * (H*P[k|k-1]*H' + R)^(-1)
    x_hat(:,k) = x_hat(:,k) + K(:,:,k)*(y(:,k)-H*x_hat(:,k));  % x[k|k] = x[k|k-1] + K[k]*(y[k]-H*x[k|k-1])
    P_hat(:,:,k) = P_hat(:,:,k) - K(:,:,k)*H*P_hat(:,:,k);     % P[k|k] = P[k|k-1] - K[k]*H*P[k|k-1]
end

end


